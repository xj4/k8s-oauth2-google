package main

import (
	"fmt"
	"net/http"
	"log"
	"gitlab.com/xj4/k8s-oauth2-google/handlers"
)

func main() {
	// We create a simple server using http.Server and run.
	server := &http.Server{
		Addr: fmt.Sprintf(":3000"),
		Handler: handlers.New(),
	}

	log.Printf("Starting HTTP Server. Listening at %q", server.Addr)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Printf("%v", err)
	} else {
		log.Println("Server closed!")
	}
}
