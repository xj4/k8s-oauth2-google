package handlers

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	//"html/template"
)

// Scopes: OAuth 2.0 scopes provide a way to limit the amount of access that is granted to an access token.
var googleOauthConfig = &oauth2.Config{
	//RedirectURL:  "http://localhost:3000/auth/google/callback",
	RedirectURL:  os.Getenv("CALL_BACK_URL"),
	ClientID:     os.Getenv("GOOGLE_OAUTH_CLIENT_ID"),
	ClientSecret: os.Getenv("GOOGLE_OAUTH_CLIENT_SECRET"),
	Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint:     google.Endpoint,
}

// K8s authen variable
var k8s_certificate_authority_data = os.Getenv("K8S_CERTIFICATE_AUTHORITY_DATA")
var k8s_api = os.Getenv("K8S_API")
var k8s_clustername = os.Getenv("K8S_CLUSTERNAME")
var k8s_client_id = os.Getenv("K8S_CLIENT_ID")

type Oauth2 struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
	Hd            string `json:"hd"`
}

type TokenResponse struct {
	IdToken string `json:"id_token"`
}

const k8s_auth = `
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: K8S_CERTIFICATE_AUTHORITY_DATA
    server: K8S_API
  name: K8S_CLUSTERNAME
contexts:
- context:
    cluster: K8S_CLUSTERNAME
    user: EMAILXYZ
  name: K8S_CLUSTERNAME
current-context: K8S_CLUSTERNAME
kind: Config
preferences: {}
users:
- name: EMAILXYZ
  user:
    auth-provider:
      config:
        client-id: K8S_CLIENT_ID
        id-token: TOKENID
        idp-issuer-url: https://accounts.google.com
        refresh-token: REFRESHTOKEN
      name: oidc
`
const oauthGoogleUrlAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

func oauthGoogleLogin(w http.ResponseWriter, r *http.Request) {

	// Create oauthState cookie
	oauthState := generateStateOauthCookie(w)

	/*
		AuthCodeURL receive state that is a token to protect the user from CSRF attacks. You must always provide a non-empty string and
		validate that it matches the the state query parameter on your redirect callback.
	*/
	u := googleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

func oauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	// Read oauthState from Cookie
	oauthState, _ := r.Cookie("oauthstate")

	if r.FormValue("state") != oauthState.Value {
		log.Println("invalid oauth google state")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	data, err := getUserDataFromGoogle(r.FormValue("code"))
	if err != nil {
		log.Println(err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

        //t, _ := template.ParseFiles("templates/success.html")
        //t.Execute(w, data)

	fmt.Fprintf(w, "%s", data)
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(20 * time.Minute)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func getUserDataFromGoogle(code string) ([]byte, error) {
	// Use code to get token and get user info from Google.
	//fmt.Println("k8s_certificate_authority_data : ", k8s_certificate_authority_data)
	token, err := googleOauthConfig.Exchange(context.Background(), code)
	//fmt.Println("token: ", token)
	//fmt.Printf("%+v\n", token)

	if err != nil {
		return nil, fmt.Errorf("code exchange wrong: %s", err.Error())
	}
	response, err := http.Get(oauthGoogleUrlAPI + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}
	IdToken := GetIDTokenWithTokenInfo(token)
	//fmt.Println("token: ", k)
	//result := strings.Replace(k8s_auth, "TOKENID", fmt.Sprintf("%s", k), -1)

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	var entries Oauth2
	json.Unmarshal(contents, &entries)
	replacer := strings.NewReplacer("TOKENID", IdToken, "EMAILXYZ", entries.Email, "K8S_CERTIFICATE_AUTHORITY_DATA", k8s_certificate_authority_data, "K8S_CLIENT_ID", k8s_client_id,"REFRESHTOKEN", token.RefreshToken,"K8S_API", k8s_api, "K8S_CLUSTERNAME",k8s_clustername)
	result := replacer.Replace(k8s_auth)
	//fmt.Println(oauthGoogleUrlAPI + token.AccessToken)
	//fmt.Println("entries", entries.Email)
	if !validateEmail(entries.Email) {
		fmt.Println("Email address is invalid: " + entries.Email)
		return []byte("403 - Permission Denied - contact: sre@xyz.vn"), nil
	} else {
		fmt.Println("Email address is VALID: " + entries.Email)
	}
	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}
	//return contents, nil
	return []byte(result), nil
}

func validateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@xyz.vn$`)
	return Re.MatchString(email)
}
func GetIDTokenWithTokenInfo(tokenInfo *oauth2.Token) string {
	rawIDToken := tokenInfo.Extra("id_token").(string)
	return rawIDToken
}
